import React, { useReducer } from 'react';

const UseReducer = () => {

    const initialState = 0;

    const reducer = (state, action) => {
        if (action.type === "INCREMENT") {
            return state = state + 1;
        }
        if (action.type === "DECREMENT") {
            return state = state - 1;
        }

    }

    const [state, dispatch] = useReducer(reducer, initialState)
    return (
        <div>
        <h4>{state}</h4>
        <button onClick = { () => dispatch({type:"INCREMENT"}) } > + </button>
        <button onClick = { () => dispatch({type:"DECREMENT"}) } > - </button>
        </div >
    )
}


export default UseReducer;