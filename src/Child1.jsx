import React, { useContext } from 'react';
import {Fname} from './UseContext';

const Child1 = () =>{


    const name = useContext(Fname);

    return(
        <div>
    <h1>{name}</h1>
        </div>
        
    )
}

export default Child1;