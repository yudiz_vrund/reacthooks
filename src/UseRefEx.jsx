import React, { useRef } from 'react'

const UseRefEx = () => {


    let nRef = useRef(null); 

    const HandleEvent = () => {
        alert(nRef.current.value);
    }

    return(
        <div>
            <input type="text" ref={nRef}></input>
            <button onClick={HandleEvent}> Submit </button>
        </div>
    )
 
}

export default UseRefEx