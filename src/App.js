import ApiEx from "./ApiEx";
import UseContext from "./UseContext";
import UseReducer from "./UseReducer";
import UseRefEx from "./UseRefEx";


function App() {
  return (<div>
    <UseReducer/>
    <UseContext/>
    <UseRefEx/>
  </div>);
}

export default App;
