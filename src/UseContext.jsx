import React, { createContext } from 'react';
import Child1 from './Child1';

const Fname = createContext();

const UseContext = () => {

    
    //const Lname = createContext();

    return (
        <div>
            <Fname.Provider value="Macboy">
                <Child1 />
            </Fname.Provider>
        </div>
    )
}


export default UseContext;
export {Fname};